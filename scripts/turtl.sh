#!/usr/bin/env bash
set -e

initialCWD=$(pwd)
mkdir -p ~/bin
cd ~/bin

wget https://github.com/turtl/desktop/releases/download/v0.7.2.5/turtl-0.7.2.5-linux64.tar.bz2
tar -xaf turtl-0.7.2.5-linux64.tar.bz2
rm turtl-0.7.2.5-linux64.tar.bz2
cd turtl-linux64
sudo ./install.sh

cd $initialCWD
