#!/usr/bin/env bash
set -e

sudo curl -L --output /usr/local/bin/gitlab-runner https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64
sudo chmod +x /usr/local/bin/gitlab-runner
sudo useradd --comment 'GitLab Runner' --create-home gitlab-runner --shell /bin/bash
sudo gitlab-runner install --user=gitlab-runner --working-directory=/home/gitlab-runner
sudo gitlab-runner start

sudo rm -f /home/gitlab-runner/.bash_logout
sudo usermod -aG docker gitlab-runner

# register runner:
# sudo gitlab-runner register --url https://gitlab.com/ --registration-token $REGISTRATION_TOKEN
