#!/usr/bin/env bash
set -e

if ! command -v python3 &> /dev/null; then
  echo "installing missing python3"
  sudo apt update
  sudo apt install -y python3
fi

./configScript.py
