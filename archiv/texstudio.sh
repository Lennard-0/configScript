#!/usr/bin/env bash
set -e

sudo add-apt-repository -y ppa:sunderme/texstudio

sudo apt install -y texlive texlive-lang-german texstudio lmodern texlive-latex-extra texlive-bibtex-extra biber

curl -s https://api.github.com/repos/JabRef/jabref/releases/latest | jq -r '.assets[] | select(.name | endswith("deb")) | .browser_download_url' | wget --input-file=- -O /tmp/jabref.deb
sudo apt install -y /tmp/jabref.deb
