#!/usr/bin/env bash
set -e

exit -1

#todo: save partition path in variable
#todo: detect partition type

lsblk
sudo mkdir /mnt/RedWD
sudo chmod go+w /mnt/RedWD
sudo mount -t ntfs-3g /dev/sdb1 /mnt/RedWD
ls /mnt/RedWD
sudo umount /mnt/RedWD
#todo check if PARTUUID was found
echo "PARTUUID=$(sudo blkid -s PARTUUID --output value /dev/sdb1) /mnt/RedWD ntfs-3g rw 0 2" | sudo tee -a /etc/fstab
sudo mount -a
ls /mnt/RedWD

lsblk
sudo mkdir /mnt/BlueWD
sudo mount -t ext4 /dev/sda1 /mnt/BlueWD
ls /mnt/BlueWD
sudo umount /mnt/BlueWD
echo "PARTUUID=$(sudo blkid -s PARTUUID --output value /dev/sda1) /mnt/BlueWD ext4 rw 0 2" | sudo tee -a /etc/fstab
sudo mount -a
sudo chmod go+w /mnt/BlueWD
ls /mnt/BlueWD


#decrease root reserved space
sudo tune2fs -l /dev/sda1  | grep "Reserved block count"
sudo tune2fs -m 0 /dev/sda1



sudo mkdir -p /mnt/IW1
echo "PARTUUID=$(sudo blkid -s PARTUUID --output value /dev/sda1) /mnt/IW1 ext4 rw 0 2" | sudo tee -a /etc/fstab
sudo mount -a
sudo chmod go+w /mnt/IW1
lsblk
